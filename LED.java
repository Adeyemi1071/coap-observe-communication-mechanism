
package coap.server;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.rest.BasicCoapResource;
import org.ws4d.coap.core.rest.CoapData;

import coap.client.Client;
import coap.server.sensor.DS28Sensor;
import coap.server.sensor.SensorValueObserver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A CoAP Resource representing the temperature measurement of a one wire sensor
 */

public class LED extends BasicCoapResource {
	static String gpioExportPattern =    "/sys/class/gpio/export";
	static String gpioValuePattern =     "/sys/class/gpio/gpio%d/value";
	static String gpioDirectionPattern = "/sys/class/gpio/gpio%d/direction";
	
	/** The index of the sensor to be read */

	public int sensor;
	
	
	public LED(String path, CoapMediaType mediaType, int sensor) {
		// Invoke super class constructor (constructor of BasicCoapResource) 
		super(path, "", mediaType);
		
		// Store the index of the sensor to be provided
		this.sensor = sensor;
		
		//Disallow POST, PUT and DELETE
		this.setDeletable(false);
    	this.setPostable(false);
    	this.setPutable(true);
    	this.setGetable(true);
    	
    	// Add some meta Information (optional)
    	this.setResourceType("LED");
    	this.setInterfaceDescription("GET");
    	this.setInterfaceDescription("PUT");
    	
    	// Run a new thread to observe sensor value changes and raise a notification
    	//new Thread(new SensorValueObserver(this, this.sensor, 250, 0.25f)).start();
	}
    
    public LED(){
    	// 		resource path,			media type					Sensor index
    	this(	"/LED", 	CoapMediaType.text_plain, 	0);
    }
    
    
    @Override
    public synchronized CoapData get(List<CoapMediaType> mediaTypesAccepted) {
		try {
			float reading = DS28Sensor.readDS18B20(this.sensor);
			if(reading>=34.6)
				return new CoapData("ON", CoapMediaType.text_plain);
			else
				return new CoapData("OFF", CoapMediaType.text_plain);
		} catch (IOException e) {
			System.err.println(e.getLocalizedMessage());
			return new CoapData("Currently there is no temperature data available!", CoapMediaType.text_plain);
		}
    }

    
	static boolean readPin(int pinNo) throws IOException {
		Path gpioPinValuePath = Paths.get(String.format(gpioValuePattern, pinNo));
		List<String> valueText;
		valueText = Files.readAllLines(gpioPinValuePath);
		if (valueText.size() == 1) {
			return Integer.parseInt(valueText.get(0)) != 0;
		} else {
			throw new IOException("Unexpected value length <> 1");
		}
	}
	static void exportPin(int pinNo) throws IOException {
		Path gpioExportPath = Paths.get(gpioExportPattern);
		Path gpioPinValuePath = Paths.get(String.format(gpioValuePattern, pinNo));
		if (!Files.exists(gpioPinValuePath)) {
			Files.write(gpioExportPath, String.valueOf(pinNo).getBytes());
    	}
	}
	
	static void setPin(int pinNo, boolean value) throws IOException {
		Path gpioPinValuePath = Paths.get(String.format(gpioValuePattern, pinNo));
		Files.write(gpioPinValuePath, value ? String.valueOf('1').getBytes() : String.valueOf('0').getBytes() );
	}

	static void setPinAsOutput(int pinNo) throws IOException {
		Path gpioPinDirPath = Paths.get(String.format(gpioDirectionPattern, pinNo));
		Files.write(gpioPinDirPath, String.valueOf("out").getBytes() );
	}
	
    
    @Override
    public synchronized CoapData get(List<String> query, List<CoapMediaType> mediaTypesAccepted) {
    	/* Just ignore query parameters for this example.*/
    	return get(mediaTypesAccepted);
    }
    
    @Override
    public synchronized boolean put(byte[] data, CoapMediaType type) {
		float reading;
		

		try {
			exportPin(18);
			setPinAsOutput(18);
			
			reading = DS28Sensor.readDS18B20(this.sensor);
			if(reading >= 34.6 && readPin(18)== false){
				setPin(18,true);
				System.out.println("==JJJJJJJJJJJJJ ===");
			}
			else if(reading < 33 && readPin(18)== true)
				setPin(18,false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return true;
	}
}

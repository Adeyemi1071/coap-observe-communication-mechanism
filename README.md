# Coap observe communication mechanism

We have two servers (Server A &B) and one client
Server A is the Temperature sensor, Server B is a LED and the client is a Raspberry Pi
Using resource constrained COAP protocol observe extension, we got the temperature from server A and based on the 
value, We changed the status of the LED.
Results were also described and analyzed using wireshark

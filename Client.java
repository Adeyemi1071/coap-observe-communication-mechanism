package coap.client;


import java.net.InetAddress;


import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.connection.BasicCoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.enumerations.CoapRequestCode;
import org.ws4d.coap.core.messages.api.CoapMessage;
import org.ws4d.coap.core.messages.api.CoapRequest;
import org.ws4d.coap.core.messages.api.CoapResponse;
import org.ws4d.coap.core.rest.CoapData;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;


public class Client implements CoapClient {
	
	private CoapClientChannel clientChannel;

	public static void main(String[] args) {
		/* FIXME 2.1 Instantiate a new Client, provide your Pi's address */
		new Client("85-c1-2c.local");
		
	}
	//////////////////////////////////////////////////////////////////
	
	
	public Client(String serverAddress) {
		try {
			/* FIXME 2.2 Create a channel to the server */
			this.clientChannel = BasicCoapChannelManager.getInstance().connect(this, InetAddress.getByName(serverAddress), CoapConstants.COAP_DEFAULT_PORT);

			if (null != this.clientChannel) {
				/* FIXME 2.3 Create a request */
				CoapRequest request = this.clientChannel.createRequest(CoapRequestCode.GET, "/temperature/sensor" ,true);
	
				/* TODO 4.2 Add the observe option to the request */
//				
				request.setObserveOption(0);
	
				/* FIXME 2.4 Send the request */
				this.clientChannel.sendMessage(request);
			} else {
				System.err.println("Connect failed: clientChannel is null!");
				System.exit(-1);
			}
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			System.exit(-1);
		}		
	}
	
	
	@Override
	public void onResponse(CoapClientChannel channel, CoapResponse response) {
		String responseMessage = "Response: " + response.toString();
		if (null != response.getPayload()) {
			responseMessage += " Payload: " + new String(response.getPayload());
			
			
			/* TODO 5.2 Add some behavior. */
			/* Get the float value from the response payload */
		   
			float temperature = Float.parseFloat(new String (response.getPayload()));

			/* Determine the new state to be sent to the server */
			
			String payload = "ON";
			
			if(temperature < 34.6) {
				payload  ="OFF";
				}
			
			
			/* Create a PUT request to "/LED" */
				
			CoapRequest request = this.clientChannel.createRequest(CoapRequestCode.PUT, "/LED" ,true);
			CoapRequest request3 = this.clientChannel.createRequest(CoapRequestCode.GET, "/LED" ,true);
			
			/* Set media-type and payload of the request */
			request.setPayload(new CoapData(payload, CoapMediaType.text_plain));
				
			request3.setPayload(new CoapData(payload, CoapMediaType.text_plain));
			
			/* Send the request */
			this.clientChannel.sendMessage(request);
			

		}		
		System.out.println(responseMessage);
		
		// TODO 4.1 Remove the folowing two lines, to prevent the client from exiting on a response.
		//System.out.println("=== STOP  Client ===");
		//System.exit(0);
	}

	@Override
	public void onConnectionFailed(CoapClientChannel channel, boolean notReachable, boolean resetByServer) {
		System.err.println("Connection Failed");
		System.exit(-1);
	}

	@Override
	public void onMCResponse(CoapClientChannel channel, CoapResponse response, InetAddress srcAddress, int srcPort) {
		/* Ignore incoming multicast messages */
	}
}

package coap.server;

import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.rest.BasicCoapResource;
import org.ws4d.coap.core.rest.CoapResourceServer;
import org.ws4d.coap.core.rest.api.CoapResource;

public class Server {
    
    public static void main(String[] args) {
 
    	/* FIXME 1.1 Instantiate a new "CoapResourceServer" */
    	CoapResourceServer resourceServer = new CoapResourceServer();
		
		/* FIXME 1.2 Instantiate a resource */
		CoapResource resource = new TemperatureResource();
		CoapResource resource2 = new Time();
		CoapResource resource3 = new LED();
		/* TODO 3.1 Make the resource observable */
    	resource.setObservable(true);
    	resource2.setObservable(true);
    	resource3.setObservable(true);
    									
		/* FIXME 1.3 Add the resource to the "CoapResourceServer" */
		resourceServer.createResource(resource);
		resourceServer.createResource(resource2);
		resourceServer.createResource(resource3);
		
	
		/* Start the server */
		try {
			/* FIXME 1.4 Run the server */
			resourceServer.start();
			
			System.out.println("=== Server Running ===");
			
		} catch (Exception e) {
		    System.err.println(e.getLocalizedMessage());
		    System.exit(1);
		}
    }
}
